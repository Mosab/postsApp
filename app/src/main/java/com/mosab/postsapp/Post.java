package com.mosab.postsapp;

import java.io.Serializable;

/**
 * Created by mosab on 7/25/2016.
 */
public class Post implements Serializable {

    // the post id
    private int mPostID ;

    // the post title
    private String mPostTitle ;

    // the post description
    private String mPostDesc;

    public Post(int id, String title, String description){
        mPostID = id ;
        mPostTitle = title ;
        mPostDesc = description ;
    }

    public int getmPostID() {
        return mPostID;
    }

    public String getmPostDesc() {
        return mPostDesc;
    }

    public String getmPostTitle() {
        return mPostTitle;
    }

    public void setmPostDesc(String desc) {
        mPostDesc = desc ;
    }

    public void setmPostTitle(String title) {
        mPostTitle = title;
    }


}
