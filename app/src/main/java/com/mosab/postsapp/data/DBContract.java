package com.mosab.postsapp.data;

import android.provider.BaseColumns;

/**
 * Created by mosab on 7/25/2016.
 */
public class DBContract {

    /*
        Inner class that defines the post table's columns
     */
    public static final class PostEntry implements BaseColumns {

        //table name
        public static final String TABLE_NAME = "post";

        // post id withen all user's posts
        public static final String _ID = "post_id";

        // the title of the post
        public static final String TITLE = "title";

        // the description of the post
        public static final String DESCRIPTION = "description";

    }

}
