package com.mosab.postsapp;

/**
 * Created by mosab on 7/26/2016.
 */

// the activity of this fragment implements this interface to communicate with the fragment
public interface OnPostSelected {

    void onPostSelected(Post post, int position);
}
