package com.mosab.postsapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;

/**
 * Created by mosab on 7/25/2016.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.PostHolder> {

    // arrayList contains the data
    private ArrayList<Post> mPosts;

    static OnPostSelected mListener;

    // adapter constructor
    public RecyclerAdapter(ArrayList<Post> posts, OnPostSelected listener) {
        mPosts = posts;

        mListener = listener ;
    }

    @Override
    public RecyclerAdapter.PostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item, parent, false);
        return new PostHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.PostHolder holder, final int position) {
        Post itemPost = mPosts.get(position);
        holder.bindPost(itemPost);

    }

    @Override
    public int getItemCount() {
        return mPosts.size() ;
    }


    /*
        inner class to implement the adapter of the recyclerView
     */
    public static class PostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mPostTitle;
        private TextView mPostDescription;
        private LinearLayout mTitleLayout ;
        private ImageView mPostImage ;
        private Post mPost ;


        private static final String POST_KEY = "POST";

        public PostHolder(View v) {
            super(v);

            mPostTitle = (TextView) v.findViewById(R.id.title_text_view);
            mPostDescription = (TextView) v.findViewById(R.id.description_text_view);
            mTitleLayout = (LinearLayout) v.findViewById(R.id.title_layout);
            mPostImage = (ImageView) v.findViewById(R.id.post_image);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onPostSelected(mPost , getAdapterPosition());
        }

        public void bindPost(Post post) {
            mPost = post;
            mPostTitle.setText(post.getmPostTitle());
            mPostDescription.setText(post.getmPostDesc());
        }


    }

    /*****************************************/




}
