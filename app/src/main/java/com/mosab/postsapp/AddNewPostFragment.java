package com.mosab.postsapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by mosab on 7/28/2016.
 */
public class AddNewPostFragment extends Fragment {

    // reference to main activity object
    MainActivity mainActivity ;

    private EditText editTitle ;
    private EditText editDescription ;

    public static AddNewPostFragment newInstance(){
        return new AddNewPostFragment();
    }

    public AddNewPostFragment(){
        // required empty constructor
    }

    @Override
    public void onStop() {
        super.onStop();

        if(mainActivity.isLandScape)
            (mainActivity.landscapeAddFAB).setImageDrawable(getResources().getDrawable(R.drawable.ic_action_name));
        else
            (mainActivity.fab).setImageDrawable(getResources().getDrawable(R.drawable.ic_action_name));
    }

    @Override
    public void onStart() {
        super.onStart();

        if(mainActivity.isLandScape)
            (mainActivity.landscapeAddFAB).setImageDrawable(getResources().getDrawable(R.drawable.finish_icon));
        else
            (mainActivity.fab).setImageDrawable(getResources().getDrawable(R.drawable.finish_icon));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (MainActivity) activity ;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.post_edit_fragment , container, false);

        editTitle = (EditText) view.findViewById(R.id.edit_title);
        editDescription = (EditText) view.findViewById(R.id.edit_description);

        editTitle.setHint(R.string.title_placeholder);
        editDescription.setHint(R.string.description_placeholder);

        return view;
    }

    public String getPostTitle(){
        return (editTitle.getText()).toString() ;
    }

    public String getPostDescription(){
        return (editDescription.getText()).toString() ;
    }

}
