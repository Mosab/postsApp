package com.mosab.postsapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by mosab on 7/29/2016.
 */
public class EditPostFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARGUMENT_POST = "post";
    private static final String ARGUMENT_POSITION = "position";

    // reference to main activity object
    MainActivity mainActivity ;

    private Post mPost ;
    private int mPosition ;

    private EditText editTitle ;
    private EditText editDescription ;

    public static EditPostFragment newInstance(Post post, int position){

        final Bundle args = new Bundle();
        args.putSerializable(ARGUMENT_POST , post);
        args.putInt(ARGUMENT_POSITION , position);
        final EditPostFragment fragment = new EditPostFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public EditPostFragment(){
        // required empty constructor
    }

    @Override
    public void onStart() {
        super.onStart();

        if( mainActivity.isLandScape ){

            mainActivity.fab.setVisibility(View.VISIBLE);
        }

        mainActivity.fab.setImageDrawable(getResources().getDrawable(R.drawable.finish_icon));


    }

    @Override
    public void onStop() {
        super.onStop();

        if( mainActivity.isLandScape )
            mainActivity.fab.setVisibility(View.INVISIBLE);

        mainActivity.fab.setImageDrawable(getResources().getDrawable(R.drawable.edit_icon));


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (MainActivity) activity ;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPost = (Post) getArguments().getSerializable(ARGUMENT_POST) ;
            mPosition = getArguments().getInt(ARGUMENT_POSITION) ;
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.post_edit_fragment , container, false);

        editTitle = (EditText) view.findViewById(R.id.edit_title);
        editDescription = (EditText) view.findViewById(R.id.edit_description);

        editTitle.setText(mPost.getmPostTitle());
        editDescription.setText(mPost.getmPostDesc());

        return view;
    }

    public String getPostTitle(){
        return (editTitle.getText()).toString() ;
    }

    public String getPostDescription(){
        return (editDescription.getText()).toString() ;
    }

    public Post getPost(){
        return mPost ;
    }
    public int getPosition(){
        return mPosition ;
    }


}
