package com.mosab.postsapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



public class PostDetailFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARGUMENT_POST = "post";

    private static final String ARGUMENT_POSITION = "position" ;

    private Post mPost;
    int mPosition ;

    MainActivity mainActivity ;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static PostDetailFragment newInstance(Post post , int position) {

        final Bundle args = new Bundle();
        args.putSerializable(ARGUMENT_POST , post);
        args.putSerializable(ARGUMENT_POSITION , position);
        final PostDetailFragment fragment = new PostDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPost = (Post) getArguments().getSerializable(ARGUMENT_POST);
            mPosition = getArguments().getInt(ARGUMENT_POSITION);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        mainActivity.delete_fab.setVisibility(View.VISIBLE);

        if( mainActivity.isLandScape )
            mainActivity.fab.setVisibility(View.VISIBLE);


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (MainActivity) activity ;

        if(!(mainActivity.isLandScape))
            mainActivity.fab.setImageDrawable(getResources().getDrawable(R.drawable.edit_icon));
    }


    @Override
    public void onStop() {
        super.onStop();

        if( mainActivity.isLandScape )
            mainActivity.fab.setVisibility(View.INVISIBLE);

        mainActivity.delete_fab.setVisibility(View.INVISIBLE);

        if(!(mainActivity.isLandScape))
            mainActivity.fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_name));

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.recyclerview_item, container, false);
        final TextView titleView = (TextView) view.findViewById(R.id.title_text_view);
        final TextView descriptionView = (TextView) view.findViewById(R.id.description_text_view);

        if(mPost!= null){
            titleView.setText(mPost.getmPostTitle());
            descriptionView.setText(mPost.getmPostDesc());

        }
        return view;
    }

    public PostDetailFragment() {
        // Required empty public constructor
    }


    public Post getPost(){
        return mPost ;
    }

    public int getPosition(){
        return mPosition ;
    }



}
