package com.mosab.postsapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity
        implements OnPostSelected {

    // reference to the current fragment in the portrait mode
    Fragment portraitCurrentFragment ;

    // tags for fragments to find the later by tag
    private static final String POST_LIST_FRAGMENT = "PostListFragment";
    private static final String POST_DETAILS_FRAGMENT = "postDetailsFragment";
    private static final String POST_EDIT_FRAGMENT = "PostEditFragment";
    private static final String POST_ADD_FRAGMENT = "postAddFragment";

    // float action buttons to handle actions for the fragments
    public FloatingActionButton fab ;
    public FloatingActionButton landscapeAddFAB ;
    public FloatingActionButton delete_fab ;

    AlertDialog.Builder alertDialogBuilder  ;

    // boolean to check if the current orientation is landscape or not
    public boolean isLandScape ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);

        // declare the Alert Dialog Builder
        alertDialogBuilder = new AlertDialog.Builder(this);


        // get reference to the delete FAB that appears when details fragment is created
        delete_fab = (FloatingActionButton) findViewById(R.id.delete_fab) ;

        fab = (FloatingActionButton) findViewById(R.id.fab);

        setFABOnClickListener();

        setDeleteFABOnClickListener();

        /**
         * check for the mode of the device (portrait or landscape)
         */
        isLandScape = isLandScapeMode() ;

        if(isLandScape){

            landscapeAddFAB = (FloatingActionButton) findViewById(R.id.add_post_fab);

            setAddFABOnClickListener();

            if (savedInstanceState == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.list_fragment, PostListFragment.newInstance() , POST_LIST_FRAGMENT)
                        .commit();

            }


        }

        else {

            if (savedInstanceState == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content_layout, PostListFragment.newInstance() , POST_LIST_FRAGMENT)
                        .commit();

                portraitCurrentFragment = getSupportFragmentManager().findFragmentByTag(POST_LIST_FRAGMENT) ;
            }

        }

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        isLandScape = isLandScapeMode();

        FragmentManager fm = getSupportFragmentManager();
        int FMSize = fm.getBackStackEntryCount();
        for(int i = 0; i < FMSize ; ++i) {
            fm.popBackStack();
        }

        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){

            setContentView(R.layout.activity_main2);

            delete_fab = (FloatingActionButton) findViewById(R.id.delete_fab) ;
            fab = (FloatingActionButton) findViewById(R.id.fab);
            landscapeAddFAB = (FloatingActionButton) findViewById(R.id.add_post_fab);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.list_fragment, PostListFragment.newInstance() , POST_LIST_FRAGMENT)
                    .commit();

            setFABOnClickListener();
            setDeleteFABOnClickListener();
            setAddFABOnClickListener();

        }

        else {

            setContentView(R.layout.activity_main2);

            delete_fab = (FloatingActionButton) findViewById(R.id.delete_fab) ;
            fab = (FloatingActionButton) findViewById(R.id.fab);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_layout , PostListFragment.newInstance() , POST_LIST_FRAGMENT)
                    .commit();

            portraitCurrentFragment = getSupportFragmentManager().findFragmentByTag(POST_LIST_FRAGMENT) ;

            setFABOnClickListener();
            setDeleteFABOnClickListener();
            setAddFABOnClickListener();
        }

    }


    private boolean isEmpytField(String field) {
        return ( field==null || field.equals("") );
    }

    private void launchFragment(int containerID , Fragment launchedFragment, String fragmentTag , int fabIcon) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerID, launchedFragment, fragmentTag)
                .addToBackStack(null)
                .commit();

        if(!isLandScape){
            portraitCurrentFragment = launchedFragment ;
            fab.setImageDrawable(getResources().getDrawable(fabIcon));
        }

    }


    @Override
    public void onPostSelected(Post post , int position) {

        final PostDetailFragment detailsFragment =
                PostDetailFragment.newInstance(post,position);

        if(isLandScape){
            getSupportFragmentManager().popBackStack();
            launchFragment( R.id.details_fragment , detailsFragment , POST_DETAILS_FRAGMENT , R.drawable.edit_icon);
        }

        else {
            launchFragment( R.id.content_layout , detailsFragment , POST_DETAILS_FRAGMENT , R.drawable.edit_icon);
        }

    }

    private void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void setFABOnClickListener() {

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isLandScape){

                    PostDetailFragment postDetailFragment = (PostDetailFragment) getSupportFragmentManager().findFragmentByTag(POST_DETAILS_FRAGMENT);
                    EditPostFragment editPostFragment = (EditPostFragment) getSupportFragmentManager().findFragmentByTag(POST_EDIT_FRAGMENT);

                    /**
                     handle action of the floating button
                     **/
                    if (postDetailFragment != null && postDetailFragment.isVisible()) {

                        getSupportFragmentManager().popBackStack();

                        launchFragment( R.id.details_fragment ,
                                EditPostFragment.newInstance(postDetailFragment.getPost(),postDetailFragment.getPosition()),
                                POST_EDIT_FRAGMENT , R.drawable.finish_icon);
                    }

                    else  if (editPostFragment != null && editPostFragment.isVisible()) {

                        Post mPost = editPostFragment.getPost();
                        String description = editPostFragment.getPostDescription().trim();
                        String title = editPostFragment.getPostTitle().trim();
                        if( isEmpytField(title) || isEmpytField(description) )
                            Toast.makeText(getApplicationContext() , "title or description can't be empty" , Toast.LENGTH_SHORT).show();
                        else{

                            ( (PostListFragment)getSupportFragmentManager().findFragmentByTag(POST_LIST_FRAGMENT) )
                                    .updateDatabase(title,description , mPost , editPostFragment.getPosition() );
                            getSupportFragmentManager().popBackStack();

                            hideKeyboard();

                            launchFragment( R.id.details_fragment ,
                                    PostDetailFragment.newInstance(editPostFragment.getPost() , editPostFragment.getPosition()),
                                    POST_EDIT_FRAGMENT , R.drawable.edit_icon);

                        }

                    }

                }

                else {
                    EditPostFragment editPostFragment = (EditPostFragment)getSupportFragmentManager().findFragmentByTag(POST_EDIT_FRAGMENT);
                    PostListFragment listPostsFragment = (PostListFragment)getSupportFragmentManager().findFragmentByTag(POST_LIST_FRAGMENT);
                    AddNewPostFragment addPostFragment = (AddNewPostFragment)getSupportFragmentManager().findFragmentByTag(POST_ADD_FRAGMENT);
                    PostDetailFragment detailsPostFragment = (PostDetailFragment)getSupportFragmentManager().findFragmentByTag(POST_DETAILS_FRAGMENT);

                    /**
                     handle action of the floating button
                     **/
                    if (editPostFragment != null && editPostFragment.isVisible()) {

                        Post mPost = editPostFragment.getPost();
                        String description = editPostFragment.getPostDescription().trim();
                        String title = editPostFragment.getPostTitle().trim();

                        if( isEmpytField(title) || isEmpytField(description) )
                            Toast.makeText(getApplicationContext() , "title or description can't be empty" , Toast.LENGTH_SHORT).show();
                        else{

                            ( (PostListFragment)getSupportFragmentManager().findFragmentByTag(POST_LIST_FRAGMENT) )
                                    .updateDatabase(title,description , mPost , editPostFragment.getPosition() );
                            getSupportFragmentManager().popBackStack();

                            fab.setImageDrawable(getResources().getDrawable(R.drawable.edit_icon));

                            portraitCurrentFragment = detailsPostFragment ;

                            hideKeyboard();
                        }

                    }

                    else if (listPostsFragment != null && listPostsFragment.isVisible()) {

                        launchFragment(R.id.content_layout , AddNewPostFragment.newInstance() , POST_ADD_FRAGMENT , R.drawable.finish_icon);


                    }

                    else if (addPostFragment != null && addPostFragment.isVisible()) {

                        String description = addPostFragment.getPostDescription().trim();
                        String title = addPostFragment.getPostTitle().trim();

                        if( isEmpytField(title) || isEmpytField(description) )
                            Toast.makeText(getApplicationContext() , "title or description can't be empty" , Toast.LENGTH_SHORT).show();

                        else{
                            ( (PostListFragment)getSupportFragmentManager().findFragmentByTag(POST_LIST_FRAGMENT) )
                                    .insertToDatabase(title,description);
                            getSupportFragmentManager().popBackStack();

                            fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_name));

                            portraitCurrentFragment = listPostsFragment ;

                            hideKeyboard();
                        }

                    }

                    else if (detailsPostFragment != null && detailsPostFragment.isVisible()) {

                        final EditPostFragment editFragment =
                                EditPostFragment.newInstance( detailsPostFragment.getPost() , detailsPostFragment.getPosition() );

                        launchFragment(R.id.content_layout , editFragment , POST_EDIT_FRAGMENT , R.drawable.finish_icon);
                    }
                }

            }
        });
    }

    private void setDeleteFABOnClickListener() {

        delete_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialogBuilder.setTitle("Delete Post...")
                        .setMessage("Do you really want to delete this post?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                PostDetailFragment currentPostFragment = (PostDetailFragment)getSupportFragmentManager().findFragmentByTag(POST_DETAILS_FRAGMENT) ;
                                ( (PostListFragment)getSupportFragmentManager().findFragmentByTag(POST_LIST_FRAGMENT) )
                                        .deleteFromDatabase(currentPostFragment.getPost() , currentPostFragment.getPosition());

                                getSupportFragmentManager().popBackStack();

                                Toast.makeText(getApplicationContext() , "delete " , Toast.LENGTH_SHORT).show();

                                if(! isLandScape){
                                    portraitCurrentFragment = getSupportFragmentManager().findFragmentByTag(POST_LIST_FRAGMENT) ;

                                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_name));
                                }
                            }})
                        .setNegativeButton(android.R.string.no, null).show();

            }

        });
    }


    private void setAddFABOnClickListener() {

        landscapeAddFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isLandScape){

                    PostListFragment postListFragment = (PostListFragment) getSupportFragmentManager()
                            .findFragmentByTag(POST_LIST_FRAGMENT);

                    AddNewPostFragment addNewPostFragment = (AddNewPostFragment) getSupportFragmentManager()
                            .findFragmentByTag(POST_ADD_FRAGMENT);

                    if (postListFragment != null && postListFragment.isVisible()) {
                        launchFragment(R.id.list_fragment , AddNewPostFragment.newInstance() , POST_ADD_FRAGMENT , -1);
                    }

                    else if (addNewPostFragment != null && addNewPostFragment.isVisible()) {
                        String description = addNewPostFragment.getPostDescription().trim();
                        String title = addNewPostFragment.getPostTitle().trim();

                        if( isEmpytField(title) || isEmpytField(description) )
                            Toast.makeText(getApplicationContext() , "title or description can't be empty" , Toast.LENGTH_SHORT).show();

                        else{
                            ( (PostListFragment)getSupportFragmentManager().findFragmentByTag(POST_LIST_FRAGMENT) )
                                    .insertToDatabase(title,description);

                            getSupportFragmentManager().popBackStack();

                            hideKeyboard();
                        }
                    }


                }
            }

        });

    }

    // return true if the device in the landscape mode
    private boolean isLandScapeMode(){
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ;
    }

}
