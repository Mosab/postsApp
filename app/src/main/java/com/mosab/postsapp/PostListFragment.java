package com.mosab.postsapp;

import com.mosab.postsapp.data.DBContract.PostEntry;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mosab.postsapp.data.DBContract;
import com.mosab.postsapp.data.DBHelper;

import java.util.ArrayList;

/**
 * Created by mosab on 7/26/2016.
 */
public class PostListFragment extends Fragment {

    // reference to the recyclerView that hold posts
    private RecyclerView mRecyclerView;
    // layoutManager for the recyclerView
    private LinearLayoutManager mLinearLayoutManager;
    // adapter of the recyclerView
    private RecyclerAdapter mAdapter;

    // ArrayList to hold the posts
    private ArrayList<Post> mPostsList ;

    // databse helper
    private DBHelper helper ;

    // database instance
    private SQLiteDatabase db ;

    // cursor to database
    private Cursor cursor;

    // offset of the database rows
    private int offset ;

    public final String LAST_ID_KEY = "last_id";

    private OnPostSelected mListener;

    public static PostListFragment newInstance() {
        return new PostListFragment();
    }

    public PostListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnPostSelected) {   //here we make sure that the activity implementing te interface (OnRageComicSelected)
            mListener = (OnPostSelected) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnRageComicSelected.");
        }

        helper = new DBHelper(context);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_posts_list, container, false);


        initiateRecyclerView(view);

        updateOffset();
        selectFromDatabase();

        return view;
    }


    private void initiateRecyclerView(View view){

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mPostsList = new ArrayList<Post>();
        mAdapter = new RecyclerAdapter(mPostsList,mListener);
        mRecyclerView.setAdapter(mAdapter);

        setRecyclerViewScrollListener();
    }


    private void setRecyclerViewScrollListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int totalItemCount = mRecyclerView.getLayoutManager().getItemCount();
                if (totalItemCount == getLastVisibleItemPosition() + 1) {
                    //loadPosts();
                }
            }
        });
    }

    private int getLastVisibleItemPosition() {
        return mLinearLayoutManager.findLastVisibleItemPosition();
    }


    public void insertToDatabase(String title,String description) {

        openDBForWrite();

        ContentValues initialValues = new ContentValues();
        initialValues.put( PostEntry.TITLE, title);
        initialValues.put( PostEntry.DESCRIPTION, description);

        if( db.insert(PostEntry.TABLE_NAME, null, initialValues)== -1 ){
            Toast.makeText(getContext(), "unable to add post with title ( "+title+" )", Toast.LENGTH_SHORT).show();

        }
        else {
            mPostsList.add(selectTheLastPost());
            mAdapter.notifyItemInserted(mPostsList.size());
            updateOffset();
        }

        closeDB();

    }

    public void deleteFromDatabase(Post post, int position) {

        openDBForWrite();

        if(db.delete(PostEntry.TABLE_NAME, PostEntry._ID + "=" + post.getmPostID(), null) > 0){
            mPostsList.remove(post);
            mAdapter.notifyItemRemoved(position);
        }
        else {
            Toast.makeText(getContext(), "unable to delete post with id ( "+post.getmPostID()+" )", Toast.LENGTH_SHORT).show();
        }

        updateOffset();

        closeDB();

    }


    public void updateDatabase(String title,String description , Post mPost , int position ){
        openDBForWrite();

        ContentValues cv = new ContentValues();
        cv.put(PostEntry.TITLE,title);
        cv.put(PostEntry.DESCRIPTION,description);

        if(db.update(PostEntry.TABLE_NAME, cv, PostEntry._ID+"=" +mPost.getmPostID(), null) >0){
            mPost.setmPostTitle(title);
            mPost.setmPostDesc(description);
            mAdapter.notifyItemChanged(position);
        }

        closeDB();

    }


    private void selectFromDatabase() {

        openDBForRead();

        // select the next 10 posts from db
        cursor = db.rawQuery("SELECT * FROM " + PostEntry.TABLE_NAME +" ORDER BY " +
                PostEntry._ID + " DESC LIMIT 10 OFFSET " + Integer.toString(offset) + " ;", null);

        if (cursor != null && cursor.moveToFirst()){ //make sure you got results, and move to first row
            do{

                int id = cursor.getInt(0);
                String title =  cursor.getString(1);
                String description = cursor.getString(2);
                mPostsList.add( new Post(id,title,description) );
                mAdapter.notifyItemInserted(mPostsList.size());
            } while (cursor.moveToNext()); //move to next row in the query result
        }

        if(cursor!=null){
            cursor.close();
        }

        closeDB();

        updateOffset();

    }


    private Post selectTheLastPost(){

        openDBForRead();

        cursor = db.rawQuery("SELECT * FROM "+ PostEntry.TABLE_NAME +" ORDER BY "+ PostEntry._ID +" DESC LIMIT 1;", null);

        cursor.moveToFirst();
        Post post = new Post(cursor.getInt(0),cursor.getString(1),cursor.getString(2));


        closeDB();

        return post;

    }


    void openDBForRead(){
        db = helper.getReadableDatabase();
    }

    void openDBForWrite(){
        db = helper.getWritableDatabase();
    }

    void closeDB(){
        if(helper != null){
            helper.close();
        }

        if(db != null){
            db.close();
        }
    }



    private void savePreferences(int postID) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(LAST_ID_KEY, postID);
        editor.apply();
    }

    private int loadSavedPreferences() {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getContext());
        return sharedPreferences.getInt(LAST_ID_KEY, 0);
    }

    private void updateOffset(){
        offset = mPostsList.size();
    }

    public Post getPostByPosition(int position){
        if(mPostsList == null)
            Toast.makeText(getContext() , "null arrayList" , Toast.LENGTH_LONG).show();
        else if(mPostsList.isEmpty())
            Toast.makeText(getContext() , "empty arrayList" , Toast.LENGTH_LONG).show();
        else
            return mPostsList.get(position);

        return null;
    }
}
